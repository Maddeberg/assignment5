<?php
include_once 'model/DBModel.php';
include_once 'model/Model.php';

class Controller {
    public $model;

    public function __construct() {
        session_start();
        $this->model = new Model();
    }

    public function invoke() {

        $skiers = $this->model->importSkiers();
        $clubs = $this->model->importClubs();
        $seasons = $this->model->importSeasons();
        $entries = $this->model->importEntries();

        $this->model = new DBModel($db=null, $skiers, $clubs, $seasons, $entries);

        $this->model->addSkier();
        $this->model->addClub();
        $this->model->addSeason();
        $this->model->addEntry();
    }
}
?>
