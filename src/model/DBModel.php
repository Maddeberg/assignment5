<?php

    class DBModel {
        protected $db = null;
        protected $skiersArray;
        protected $clubArray;
        protected $seasonArray;
        protected $entriesArray;

        function __construct($db=null, $skiers, $clubs, $seasons, $entries) {

            $this->skiersArray = $skiers;
            $this->clubArray = $clubs;
            $this->seasonArray = $seasons;
            $this->entriesArray = $entries;

            if ($db) {
                $this->db = $db;
            } else {
                $this->db = new PDO('mysql:host=127.0.0.1;dbname=assigment5;charset=utf8', 'root', '');
            }
        }

        public function addSkier() {
            $stmt = $this->db->prepare('INSERT INTO skier(Username, Firstname, Lastname, YearOfBirth) VALUES (:Username, :Firstname, :Lastname, :YearOfBirth)');
            foreach ($this->skiersArray as $skier) {
                $stmt->bindParam(':Username', $skier->username, PDO::PARAM_STR);
                $stmt->bindParam(':Firstname', $skier->firstname, PDO::PARAM_STR);
                $stmt->bindParam(':Lastname', $skier->lastname, PDO::PARAM_STR);
                $stmt->bindParam(':YearOfBirth', $skier->yearofbirth, PDO::PARAM_INT);
                $stmt->execute();
            }        }

        public function addClub() {
            $countystmt = $this->db->prepare('INSERT INTO county(City, County) VALUES (:City, :County)');
            $clubstmt = $this->db->prepare('INSERT INTO skiclub(ID, Clubname, City) VALUES (:ID, :Clubname, :City)');

            foreach ($this->clubArray as $club) {
                $countystmt->bindParam(':City', $club->city, PDO::PARAM_STR);
                $countystmt->bindParam(':County', $club->county, PDO::PARAM_STR);
                $countystmt->execute();

                $clubstmt->bindParam(':ID', $club->id, PDO::PARAM_STR);
                $clubstmt->bindParam(':Clubname', $club->name, PDO::PARAM_STR);
                $clubstmt->bindParam(':City', $club->city, PDO::PARAM_STR);
                $clubstmt->execute();
            }
        }

        public function addSeason() {
            $seasonstmt = $this->db->prepare('INSERT INTO season(FallYear) VALUES (:FallYear)');
            $skierclubstmt = $this->db->prepare('INSERT INTO skierclubyear(Username, ID, Year) VALUES (:Username, :ID, :Year)');
            $seasondiststmt = $this->db->prepare('INSERT INTO seasonskierdist(FallYear, Username, TotalDist) VALUES (:FallYear, :Username, :TotalDist)');

            foreach ($this->seasonArray as $season) {
                $seasonstmt->bindParam(':FallYear', $season->fallyear, PDO::PARAM_INT);
                $seasonstmt->execute();

                $skierclubstmt->bindParam(':Username', $season->username, PDO::PARAM_STR);
                $skierclubstmt->bindParam(':ID', $season->clubid, PDO::PARAM_STR);
                $skierclubstmt->bindParam(':Year', $season->fallyear, PDO::PARAM_INT);
                $skierclubstmt->execute();

                $seasondiststmt->bindParam(':FallYear', $season->fallyear, PDO::PARAM_INT);
                $seasondiststmt->bindParam(':Username', $season->username, PDO::PARAM_STR);
                $seasondiststmt->bindParam(':TotalDist', $season->totaldistance, PDO::PARAM_INT);
                $seasondiststmt->execute();
            }
        }

        public function addEntry() {
            $entrystmt = $this->db->prepare('INSERT INTO entry(Date, Area, Distance, Username) VALUES (:Date, :Area, :Distance, :Username)');

            foreach ($this->entriesArray as $entry) {
                $entrystmt->bindParam(':Date', $entry->date, PDO::PARAM_INT);
                $entrystmt->bindParam(':Area', $entry->area, PDO::PARAM_STR);
                $entrystmt->bindParam(':Distance', $entry->distance, PDO::PARAM_INT);
                $entrystmt->bindParam(':Username', $entry->username, PDO::PARAM_STR);
                $entrystmt->execute();
            }
            echo count($this->entriesArray);
        }
    }
?>
