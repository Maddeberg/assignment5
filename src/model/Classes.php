<?php 
    
/**
* Skier
*/
class Skier {
    public $username;
    public $firstname;
    public $lastname;
    public $yearofbirth;
    
    /**
     * Constructor for skier
     * @param string $username    Skier's username
     * @param string $firstname   Skier's first name
     * @param string $lastname    Skier's last name
     * @param itn $yearofbirth Skier's year of birth
     */ 
    public function __construct($username, $firstname, $lastname, $yearofbirth) {
        $this->username = $username;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->yearofbirth = $yearofbirth;
    }
}

/**
* Club
*/
class Club {
    public $id;
    public $name;
    public $city;
    public $county;

    /**
     * Constructor for club
     * @param string $id     Club's id
     * @param string $name   Club's name
     * @param string $city   City where club is
     * @param string $county County where club's city is
     */
    public function __construct($id, $name, $city, $county) {
        $this->id = $id;
        $this->name = $name;
        $this->city = $city;
        $this->county = $county;
    }
}

/**
* Season
*/
class Season {
    public $fallyear;
    public $clubid;
    public $username;
    public $totaldistance;

    /**
     * Constructor for season
     * @param int $fallyear         The year of the season
     * @param string $clubid        Club's id
     * @param string $username      Skier's username
     * @param int $totaldistance    Total distance of skier's entries
     */ 
    public function __construct($fallyear, $clubid, $username, $totaldistance) {
        $this->fallyear = $fallyear;
        $this->clubid = $clubid;
        $this->username = $username;
        $this->totaldistance = $totaldistance;
    }
}

/**
* Entry
*/
class Entry {
    public $date;
    public $area;
    public $distance;
    public $username;

    /**
     * Constructor for entry
     * @param date $date        Date of entry
     * @param string $area      Entry's area
     * @param int $distance     Entry's distance
     * @param string $username  Skier's username
     */
    function __construct($date, $area, $distance, $username) {
        $this->date = $date;
        $this->area = $area;
        $this->distance = $distance;
        $this->username = $username;

    }
}
?>