<?php

include_once 'Classes.php';

class Model {
    protected $doc;
    protected $xpath;

    public function __construct() {
        $this->doc = new DOMDocument();
        $this->doc->load('../SkierLogs.xml');
        $this->xpath = new DOMXPath($this->doc);
    }

    public function importSkiers() {
        $SkiersArray = array();
        $skiers = $this->xpath->query('/SkierLogs/Skiers/Skier');

        foreach ($skiers as $skier) {
            $username = $skier->getAttribute('userName');
            $firstname = $skier->getElementsByTagName('FirstName')->item(0)->textContent;
            $lastname = $skier->getElementsByTagName('LastName')->item(0)->textContent;
            $yearofbirth = $skier->getElementsByTagName('YearOfBirth')->item(0)->textContent;

            $SkiersArray[] = new Skier($username, $firstname, $lastname, $yearofbirth);
        }

        return $SkiersArray;
    }

    public function importClubs() {
        $ClubsArray = array();
        $clubs = $this->xpath->query('/SkierLogs/Clubs/Club');

        foreach ($clubs as $club) {
            $id = $club->getAttribute('id');
            $name = $club->getElementsByTagName('Name')->item(0)->textContent;
            $city = $club->getElementsByTagName('City')->item(0)->textContent;
            $county = $club->getElementsByTagName('County')->item(0)->nodeValue;

            $ClubsArray[] = new Club($id, $name, $city, $county);
        }
        return $ClubsArray;
    }

    public function importSeasons() {
        $SeasonsArray = array();
        $seasons = $this->xpath->query('/SkierLogs/Season');

        foreach($seasons as $season) {
            $fallyear = $season->getAttribute('fallYear');
            $clubs = $season->getElementsByTagName('Skiers');
            foreach($clubs as $club) {
                $clubid = $club->getAttribute('clubId');
                $skiers = $club->getElementsByTagName('Skier');
                foreach($skiers as $skier){
                    $username = $skier->getAttribute('userName');
                    $entries = $skier->getElementsByTagName('Entry');
                    $totaldistance = 0;
                    foreach($entries as $entry) {
                        $distance = $entry->getElementsByTagName('Distance')->item(0)->nodeValue;
                        $totaldistance += $distance;
                    }
                $SeasonsArray[] = new Season($fallyear, $clubid, $username, $totaldistance);
                }
            }

        }
        return $SeasonsArray;
    }

    public function importEntries() {
        $EntriesArray = array();
        $skiers = $this->xpath->query('/SkierLogs/Season/Skiers/Skier');

        foreach ($skiers as $skier) {
            $username = $skier->getAttribute('userName');
            $entries = $skier->getElementsByTagName('Entry');
            foreach ($entries as $entry) {
                $date = $entry->getElementsByTagName('Date')->item(0)->textContent;
                $area = $entry->getElementsByTagName('Area')->item(0)->textContent;
                $distance = $entry->getElementsByTagName('Distance')->item(0)->nodeValue;
                $EntriesArray[] = new Entry($date, $area, $distance, $username);
            }

        }
        return $EntriesArray;
    }
}
?>
